/**
* @typedef RGB_InputFields
* @property {WebElement} r
* @property {WebElement} g
* @property {WebElement} b
*/
describe("UI Tests", () => {
    /** @type {RGB_InputFields} */
    let sliders = { r: undefined, g: undefined, b: undefined };
    it("Can find all rgb sliders", async () => {
        sliders = {
            r: await driver.findElement(By.id('slider-r')),
            g: await driver.findElement(By.id('slider-g')),
            b: await driver.findElement(By.id('slider-b'))
        };
    });
    it('Can get and set "value-rgb" input field', async () => {
        const box_rgb = await driver.findElement(By.id("value-rgb"));
        let rgb_value = await box_rgb.getAttribute('value');
        expect(rgb_value).to.eq("rgb(0, 255, 0)");
        const keystroke_sequence = [];
        // Käyttäjä pyyhkii tietokentän tiedot
        for (let i = 0; i < rgb_value.length; i++) {
            keystroke_sequence.push(Key.BACK_SPACE);
        }
        // Käyttäjä kirjoittaa arvon
        keystroke_sequence.push("rgb(255, 255, 0)");
        // Käyttäjä painaa enter
        keystroke_sequence.push(Key.ENTER);
        // Sekvenssi kirjoitetaan kenttään (suoritus)
        await box_rgb.sendKeys(...keystroke_sequence);
        rgb_value = await box_rgb.getAttribute('value');
        expect(rgb_value).to.eq("rgb(255, 255, 0)");
        await sleep(5);
    });
    it('Can move sliders', async () => {
        const slider_r = await driver.findElement(By.id("slider-r"));
        const slider_g = await driver.findElement(By.id("slider-g"));
        const slider_b = await driver.findElement(By.id("slider-b"));
        await slider_r.sendKeys(Key.LEFT.repeat(300));
        await slider_g.sendKeys(Key.LEFT.repeat(300));
        await slider_b.sendKeys(Key.LEFT.repeat(300));
        await sleep(5);
    });
});


