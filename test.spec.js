const expect = require('chai').expect;
const { Builder, By, Key, until, ThenableWebDriver, WebElement } = require('selenium-webdriver');
const BASE_URL = "https://niisku.lab.fi/~kardev/projects/color-picker/"
describe("UI Tests", () => {
    /** @type {ThenableWebDriver} */
    let driver = undefined;
    before(async () => {
        driver = await new Builder().forBrowser('firefox').build(); // launch firefox
        await driver.get(BASE_URL); // load website
    });
    it("Can find all rgb sliders", async () => { });
    it("Can find all rgb inputs", async () => { });
    it("Can get hex value from 'value-hex' input field", async () => { });
    it("Can get and set 'value-rgb' input field", async () => { });
    it("Can move slider", async () => { });
    it('Can find all rgb inputs', async () => {
        const value_r = await driver.findElement(By.id("value-r"));
        const value_g = await driver.findElement(By.id("value-g"));
        const value_b = await driver.findElement(By.id("value-b"));
    });
    it('Can get hex value from "value-hex" input field',
        async () => {
            const hex = await driver
                .findElement(By.id("value-hex"))
                .getAttribute('value');
            expect(hex).to.eq("#00ff00");
        });
    after(async () => {
        await driver.close();
    });
});